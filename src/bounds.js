/**
 *
 */

function getMapBounds(coords) {
	if (coords.length === 0) {
		return [[45, 0], [-45, 0]];
	}

	let points = [];

	coords.forEach(coord => {
		points.push([coord.lat, coord.lng]);
	});

	return points;
}

export { getMapBounds };
