import React from 'react'
import ReactDOM from 'react-dom'
import './index.css'
import 'purecss'
import * as serviceWorker from './serviceWorker'
import { BrowserRouter, Switch, Route } from 'react-router-dom'

import App from './App'
import Landing from './components/Landing'
import MapContainer from './components/MapContainer'

ReactDOM.render((
    <App>
        <BrowserRouter>
            <Switch>
                <Route exact path="/" component={Landing}/>
                <Route path="/map" component={MapContainer} />
            </Switch>
        </BrowserRouter>
    </App>

), document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
