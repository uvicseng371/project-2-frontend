import axios from 'axios'
/**
 * getFireCoordinates
 * TODO: Implement actual functionality
 * @return JSON
 *  - metadata, some format TBD
 *  - coords: array of objects
 *      - object: {x: int, y: int, radius: int}
 */

async function getFireCoordinates() {
	const timestamp = Math.round((new Date()).getTime() / 1000) - 86400
	return axios.get('https://api.forestfyre.xyz/fires?start_timestamp=' + timestamp)
		.then(function (response) {
			// handle success
			console.log(response);
			return response.data
		})
		.catch(function (error) {
			// handle error
			console.log(error);
		})
	}
/*	
const coordinates = {
	metadata: "Some data",
	coords: [
		{
			lat: 50.024444,
			lng: -125.2475,
			radius: 1.5,
			notes: "this fire is lit"
		},
		{ lat: 49.143889, lng: -125.891667, radius: 1.5 },
		{ lat: 49.753056, lng: -125.296389, radius: 1.5 },
		{ lat: 48.4073, lng: -123.3298 }
	]
};*/
export { getFireCoordinates };
