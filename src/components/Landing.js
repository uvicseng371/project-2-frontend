import React, { Component } from "react";
import { Link } from "react-router-dom";

class Landing extends Component {
	render() {
		return (
			<div className="background">
				<div className="flex-top">
					<h1>
						<strong>ForestFyre</strong>
					</h1>
				</div>
				<div className="flex-landing">
					<div className="landing-box">
						<h1>What is this app?</h1>
						<hr />
						<p>
							This is a project for the Software Evolution class at UVic (SENG 371).
							It is a proof of concept and does not indicate the location of any
							actual forest fires.
						</p>

						<Link to="map">
							<button className="pure-button">Go to the app!</button>
						</Link>
					</div>
				</div>
				<div className="flex-bottom">
					Copyright (c) Tyler Harnadek, Jon Pavelich, Joel Kerfoot, Sean Burt
				</div>
			</div>
		);
	}
}

export default Landing;
