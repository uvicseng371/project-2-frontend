import React, { Component } from "react"
import { Map, TileLayer, Marker, Popup, Circle } from "react-leaflet"
import L from "leaflet"
import { getFireCoordinates } from "../api.js"
import { getMapBounds } from "../bounds.js"
import logo from "../logo.png"

const fireIcon = L.icon({
	iconUrl: logo,
	iconSize: [35, 35], // size of the icon
	iconAnchor: [17, 30] // point of the icon which will correspond to marker's location
})

class MapContainer extends Component {
	state = {
		fires: []
	}

	componentDidMount() {
		getFireCoordinates().then(results => {
			this.setState({
				fires: results ? results.coords : []
			})
		})
	}

	render() {
		const bounds = getMapBounds(this.state.fires)
		const circles = this.state.fires.map(fire => {
			let color = "rgb(246, 240, 82)"
			if (1000 <= fire.radius && fire.radius < 2500) color = "rgb(241, 188, 49)"
			else if (2500 <= fire.radius && fire.radius < 5000) color = "rgb(226, 88, 34)"
			else if (5000 <= fire.radius && fire.radius < 10000) color = "rgb(178, 34, 34)"
			else if (10000 <= fire.radius && fire.radius < 15000) color = "rgb(124, 10, 2)"

			return <Circle center={fire} color={color} radius={fire.radius} />
		})
		const markers = this.state.fires.map(fire => {
			return (
				<Marker key={fire.id} position={fire} icon={fireIcon}>
					{fire.note != null && (
						<Popup>
							{fire.note} <br />
						</Popup>
					)}
				</Marker>
			)
		})
		return (
			<div className="leaflet-container">
				<Map bounds={bounds} boundsOptions={{ padding: [50, 50] }}>
					<TileLayer
						attribution='&amp;copy <a href="https://opentopomap.org/about">OpenTopoMap</a> contributors'
						url="https://{s}.tile.opentopomap.org/{z}/{x}/{y}.png"
					/>
					{circles}
					{markers}
				</Map>
			</div>
		)
	}
}

export default MapContainer
